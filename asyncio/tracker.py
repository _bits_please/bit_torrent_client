import ipaddress
import socket
import struct
from urllib import parse as urlparse
from hashlib import sha1
from bencoding import Encoder, Decoder

import aiohttp
import bencoder
# import yarl

from torrent import Torrent
from util import LOG, PEER_ID


class Tracker(object):
    def __init__(self, torrent ):
        self.torrent = torrent
        self.tracker_url = torrent[b'announce'].decode('utf-8')
        self.peers = []

    async def get_peers(self):
        peers_resp = await self.request_peers()
        peers = self.parse_peers(peers_resp[b'peers'])
        return peers

    async def request_peers(self):
        async with aiohttp.ClientSession() as session:
            resp = await session.get(self.tracker_url, params=self._get_request_params())
            resp_data = await resp.read()
            peers = None
            peers = bencoder.decode(resp_data)
            print('printing peers-------------')
            print(peers)
            return peers

    def _get_request_params(self):
        info = self.torrent[b'info']
        if b'length' in info:
            size = int(info[b'length'])
        else:
            size = sum([int(f[b'length']) for f in info[b'files']])
        return {
            'info_hash': sha1(Encoder(self.torrent[b'info']).encode()).digest(),
            'peer_id': PEER_ID,
            'compact': 1,
            'no_peer_id': 0,
            'event': 'started',
            'port': 59696,
            'uploaded': 0,
            'downloaded': 0,
            'left': size
        }

    def parse_peers(self, peers : bytes):
        peer = []
        binary_ip = peers
        for i in range(len(binary_ip)//6):
            offset = 6*i
            ip1 = struct.unpack_from("!i", binary_ip, offset)[0] # ! = network order(big endian); i = int
            first_ip = socket.inet_ntoa(struct.pack("!i", ip1))
            offset += 4 # save where the first ip ends and the port begins
            port1 = struct.unpack_from("!H", binary_ip, offset)[0] # H = unsigned short
            peer.append((ip1,port1))
        return(peer)

