import asyncio
import os

class FileSaver(object):
    

    
    def __init__(self, torrent):
        name = torrent[b'info'][b'name'].decode()
        self.file_name = os.path.join('.', name)
        ##open the file for read write or create a new
        self.fd = os.open(self.file_name, os.O_RDWR | os.O_CREAT)
        self.received_blocks_queue = asyncio.Queue()
        asyncio.ensure_future(self.start())

    def get_received_blocks_queue(self):
         return self.received_blocks_queue

    async def start(self):
        while True:
            block = await self.received_blocks_queue.get()

            block_abs_location, block_data = block
            os.lseek(self.fd, block_abs_location, os.SEEK_SET)
            os.write(self.fd, block_data)
