
# Bit Torrent Client #
Part of Networking Course at IIT Palakkad. 

Project by:
- Ashutosh Upadhye
- Amish Ranjan
- Vishal Kumar Chaudhary
- Vishwajeet Kawale

Supervised by:
- Prof Piyush Kurur

### Overview ###
* This is the BitBucket repository for our Project, Bit Torrent Client. The client is expected to download files from multiple peers who are seeding the file using the peer to peer Bit Torrent Protocol. 
* Version 1.0 
	* This is very much like the alpha version of the torrent client. 
### How do I set up? ###
* To connect to the peers you might want to consider using a VPN, especially if you're trying to execute the scripts from an Institution which has blocked the Bit Torrent Protocol.
* Python version required: Python3.6
* The following python dependicies should work with Python 3.6: bencoding, hashlib, requests, random, socket, asyncio, bitstring. 
### Running the test program ###
Execute the following command in terminal.   
```python3.6 main.py <complete_address/to/file.torrent>```    
By default, the program tries to download ```Ubuntu.iso```  
### Current Status###
#### We are able to achieve the following:  
* Parsing ```.torrent``` file.   
* Getting the addresses of all the peers from ```announce url```.  
* Connecting to all the peers.   
* An indepedent thread would run for each peer.   
* Sending Handshake Message to peers.   
* Receiving CHOKE/UNCHOKE.  
* Sending INTERESTED message to Unchoked peers.   
* Receiving messages from peers and decoding the types.  
* Requesting for pieces from peers sending HAVE message.   
* Receiving bitfield (data) from peers.  

#### What we have not accomplished is the following.  
* We are unable to merge different data packets into one file appropriately. 
### Torrio.py ###
We have also tried to implement using asyncio, which is still incomplete, however for some specific ```.torrent``` files it works. 	  
To run torrio.py, execute  
```python3.6 torrio.py <filename>```

### Licence ###
This project is under APACHE 2.0 Licence. Feel free to use it the way you want. 

### Acknowledgements ###
We have used ```bencoding.py```, an implementation of Markus Eliasson for binary encoding.   
We have referred to the following posts frequently.   
- http://www.kristenwidman.com/blog/33/how-to-write-a-bittorrent-client-part-1/   
- http://bittorrent.org/beps/bep_0003.html  