#!/usr/bin/python

# We have used bencoding.py, an implementation of Markus Eliasson. 

# imports from various libraries.
from bencoding import Decoder ,Encoder
from hashlib import sha1
import requests
import random
import struct
import socket
import string
import asyncio
import bitstring
import _thread
import threading

# Creates the parameters for connecting to the tracker.
# Also parses torrent file. 
def tracker(filename = 'ubuntu-16.04.4-desktop-amd64.iso.torrent'):
	
	torrent = ""
	with open(filename,"rb") as f :
		torrent = f.read()
		torrent = (Decoder(torrent).decode())

	# Getting the URL of the tracker. 
	tracker = torrent[b'announce'].decode('utf-8')
	
	# Creating parameters for the tracker. 
	info = Encoder(torrent[b'info']).encode()
	info_hash = sha1(info).digest()

	pl = {}
	pl['uploaded'] = 0
	pl['downloaded'] = 0
	pl['event'] = 'started'
	pl['peer_id'] = '-PC0001-' + ''.join([str(random.randint(0, 9)) for _ in range(12)])
	pl['info_hash'] = info_hash
	pl['left'] = 0
	pl['port'] = 6881
	pl['compact'] = 1

	# Requesting the tracker for a list of peers. 
	r = requests.get(tracker, params=pl)

	decoded = Decoder(r._content).decode() # decode the bencoded announce
	binary_ip = decoded[b'peers']

	# Decoding response from the tracker. 
	# Contains peers' IPs and Ports.
	peer = []
	for i in range(len(binary_ip)//6):
		offset = 6*i
		ip1 = struct.unpack_from("!i", binary_ip, offset)[0] # ! = network order(big endian); i = int
		first_ip = socket.inet_ntoa(struct.pack("!i", ip1))
		offset += 4 # save where the first ip ends and the port begins
		port1 = struct.unpack_from("!H", binary_ip, offset)[0] # H = unsigned short
		peer.append((first_ip,port1))

	return(torrent, pl , peer)

# Creating the handshake message for the peers. 
def handshake_mssg(pl, torrent):
	# Parameters are packed into handshake message.  
	PEER_ID = pl['peer_id']
	
	info = Encoder(torrent[b'info']).encode()
	info_hash = sha1(info).digest()
	
	# Peer ID corresponds to the client. 
	handshake_msg = struct.pack(
			'>B19s8x20s20s',
			19,
			b'BitTorrent protocol',
			info_hash,
			PEER_ID.encode()
			)
	return(handshake_msg)

# Decodes the received Handshake message. 
def decode_handshake_reply(data):
	# Avoids unexpected stopping of threads. 
	try:
		data_dec = struct.unpack('>B19s8x20s20s',data)
		return(data_dec)
	except:
		return((19, b'BitTorrent protocol', b'w\x8c\xe2\x80\xb5\x95\xe5w\x80\xff\x08?.\xb6\xf8\x97\xdf\xa4\xa4\xee', b'-TR2920-9j4m4m8tpgtt'))


# Checks the validity of the response from the peer. 
# Checks for the handshake response only. 
def check_data(pl, data):
	if(data[0]==19 and data[1] == b'BitTorrent protocol' and data[2]  == pl['info_hash'] ):
		return(True)
	else:
		return(False)


# Tries to receive the data based on different conditions. 
# Runs in an independent thread for each peer. 
def recieve_data(sock, peer):
	while True:
		buf = sock.recv(4096)
		
		# The length should be greater than 4. 
		if len(buf) < 4:
			break
		length = struct.unpack('>I', buf[0:4])[0]

		if not len(buf) >= length:
			break

		if length == 0:
			print('[Message] KEEP ALIVE from peer: ', peer)
			continue

		if len(buf) < 5:
			print('Buffer is less than 5... breaking')
			break

		msg_id = struct.unpack('>b', buf[4:5])[0] # 5th byte is the ID

		if msg_id == 0:
			print('[Message] CHOKE from peer: ', peer)
			
		elif msg_id == 1:
			print('[Message] UNCHOKE from peer: ', peer)
			interested_mssg  = struct.pack('>Ib', 1, 2)
			sock.send(interested_mssg)
			print(interested_mssg[0:4])

			
		elif msg_id == 2:
			print('[Message] INTERESTED from peer: ', peer)
			pass

		elif msg_id == 3:
			print('[Message] NOT INTERESTED from peer: ', peer)
			pass

		elif msg_id == 4:
			buf = buf[5:]
			print('[Message] HAVE from peer: ', peer)
			# msg = struct.pack('>Ib', 1, 2)
			# sock.send(msg)
			pass

		elif msg_id == 5:
			bitfield = buf[5: 5 + length - 1]
			have_pieces = bitstring.BitArray(bitfield)
			print('[Message] Bitfield: {}'.format(bitfield[:10]))

			
		elif msg_id == 7:
			print ("Message ID = 7")
			l = struct.unpack('>I', data[:4])[0]
			try:
				parts = struct.unpack(
					'>IbII' + str(l - 9) + 's',
					data[:length + 4])
				piece_idx, begin, data = parts[2], parts[3], parts[4]
			except struct.error:
				print('error decoding piece')
				return None


# Makes connection for individual threads. 
# Runs on different threads. 
def make_one_connection(pl, peer, handshake_msg):
	print('------------peer in action :- ', peer)
	sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	sock.connect(peer)
	sock.send(handshake_msg)
	try:
		data1 = sock.recv(4096)
		decoded_data = decode_handshake_reply(data1)
		if(check_data(pl,decoded_data)== True):
			print('--------recieving data from peer:- ', peer)
			recieve_data(sock, peer)
		else:
			print('connection not matched')			
	except:
		print ('***************make connection failed')
		pass


# For thread implementation.
class myThread (threading.Thread):
	def __init__(self, pl, peer, handshake_msg):
		threading.Thread.__init__(self)
		self.pl = pl
		self.peer = peer
		self.handshake_msg = handshake_msg
	def run(self):
		make_one_connection(self.pl, self.peer, self.handshake_msg)


##########################################################
# Driver code. 
##########################################################

try:
	torrent, pl , peer = tracker(argv[1])
except:
	torrent, pl , peer = tracker()
handshake_msg = handshake_mssg(pl, torrent)

ThreadArray = []
for i in peer:
	print (i)
	ThreadArray.append(myThread( pl, i, handshake_msg))

for	i in ThreadArray:
	try:
		i.start()
	except:
		print('Garbage received from peer: ', peer)


